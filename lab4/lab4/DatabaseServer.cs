﻿using System;
using System.IO.Pipes;
using System.Threading;

namespace lab4
{
    enum Command
    {
        ConnectRead = 1,
        ConnectWrite = 2,
        ReaderEndReading = 3,
        Write = 4,
        EndWriting = 5
    }

    enum Response
    {
        CanRead = 1,
        CanWrite = 2,
        CantConnectToWrite = 3,
        EndWrite = 4,
        EndRead = 5
    } 

    class DatabaseServer
    {
        private int _elementsCount;
        private int _readersAtMoment;
        private bool _isSomeoneWriting;

        public DatabaseServer()
        {
            _elementsCount = 0;
            _readersAtMoment = 0;
            _isSomeoneWriting = false;
        }

        

        public void Start()
        {
            Console.WriteLine(Constants.DATABASE_CREATED + Constants.PIPE_NAME);
            while (true)
            {
                Connector.CreateServerConnection();
                Connector.WaitForConnectionOnServer();
                try
                {
                    var command = (Command)Connector.ReadByteOnServer();
                    switch (command)
                    {
                        case Command.ConnectRead:
                            OnReaderCome();
                            break;
                        case Command.ReaderEndReading:
                            OnReaderEndReading();
                            break;
                        case Command.ConnectWrite:
                            OnWriterCome();
                            break;
                        case Command.Write:
                            OnWriterWrite();
                            break;
                        case Command.EndWriting:
                            OnEndWriting();
                            Console.WriteLine("asjdaskjdasd");
                            break;
                        default:
                            Console.WriteLine(Constants.WRONG_COMMAND);
                            break;
                    }
                    Connector.WaitForDrainOnServer();
                    PrintStatus();
                    
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }

            }
        }

        private void OnReaderCome()
        {
            _readersAtMoment++;
            Connector.WriteByteOnServer((byte)Response.CanRead);
        }

        private void OnWriterCome()
        {
            if (!_isSomeoneWriting)
            {
                _isSomeoneWriting = true;
                Connector.WriteByteOnServer((byte)Response.CanWrite);
            }
            else
            {
                Connector.WriteByteOnServer((byte)Response.CantConnectToWrite);
            }
        }

        private void OnWriterWrite()
        {
            _elementsCount++;
            Connector.WriteByteOnServer((byte)Response.EndWrite);
        }

        private void OnReaderEndReading()
        {
            _readersAtMoment--;
            Connector.WriteByteOnServer((byte)Response.EndRead);
        }

        private void OnEndWriting()
        {
            _isSomeoneWriting = false;
            Connector.WriteByteOnServer((byte)Response.EndWrite);
        }

        private void PrintStatus()
        {
            Console.WriteLine("STATUS : Elements = {0}, readers = {1} !", _elementsCount, _readersAtMoment);
        }

    }
}
