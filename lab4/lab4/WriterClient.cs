﻿using System;
using System.Threading;

namespace lab4
{
    class WriterClient
    {
        private int _index;

        public WriterClient(int index)
        {
            _index = index;
        }

        public void Start()
        {
            Console.WriteLine("Writer {0} created with pipe : {1}", _index, Constants.PIPE_NAME);
            while (true)
            {
                var random = new Random();
                try
                {
                    Thread.Sleep(100);
                    Console.WriteLine("Writer {0} is ready to some work", _index);
                    var response = Connector.SendCommandFromClient((byte)Command.ConnectWrite);
                    while ((response == (int)Response.CantConnectToWrite))
                    {
                        Console.WriteLine("Writer {0} cant connect to write and thrown mouse into wall !", _index);
                        Thread.Sleep(random.Next(1000, 5000));
                        response = Connector.SendCommandFromClient((byte)Command.ConnectWrite);
                    }
                    Console.WriteLine("Writer {0} gets into database in write mode and start writing.", _index);
                    for (int i = 0; i < 5; i++)
                    {
                        Connector.SendCommandFromClient((byte)Command.Write);
                        Thread.Sleep(1000);
                    }
                    Connector.SendCommandFromClient((byte)Command.EndWriting);
                    Console.WriteLine("Writer {0} end writing and go smoking", _index);
                    Thread.Sleep(random.Next(5000, 10000));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }


            }
        }
    }
}
