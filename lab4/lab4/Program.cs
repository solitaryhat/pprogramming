﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        static void ArgumentsError()
        {
            Console.WriteLine("Use:");
            Console.WriteLine("lab4.exe server");
            Console.WriteLine("lab4.exe reader <index>");
            Console.WriteLine("lab4.exe writer <index>");
            Environment.Exit(1);
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                ArgumentsError();
            }
            switch (args[0])
            {
                case "server":
                    var dbServer = new DatabaseServer();
                    dbServer.Start();
                    break;
                case "reader":
                    if (args.Length < 2)
                    {
                        ArgumentsError();
                    }
                    var reader = new ReaderClient(int.Parse(args[1]));
                    reader.Start();
                    break;
                case "writer":
                    if (args.Length < 2)
                    {
                        ArgumentsError();
                    }
                    var writer = new WriterClient(int.Parse(args[1]));
                    writer.Start();
                    break;
                default:
                    break;
            }
        }
    }
}
