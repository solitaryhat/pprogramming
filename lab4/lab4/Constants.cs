﻿namespace lab4
{
    static class Constants
    {
        public const string PIPE_NAME = "lab5_ultra_pipe";
        public const string DATABASE_CREATED = "Database created with pipe : ";
        public const string WRONG_COMMAND = "Unknown command";
        public const string CANT_CONNECT = "Cant connect to pipe";
    }
}
