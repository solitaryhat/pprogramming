﻿using System;
using System.Threading;


namespace lab4
{
    class ReaderClient
    {
        private int _index;

        public ReaderClient(int index)
        {
            _index = index;
        }

        public void Start()
        {
            Console.WriteLine("Reader {0} created with pipe : {1}", _index, Constants.PIPE_NAME);
            while (true)
            {
                var random = new Random();
                try
                {
                    Thread.Sleep(100);
                    Console.WriteLine("Reader {0} is ready to some work", _index);
                    Connector.SendCommandFromClient((byte)Command.ConnectRead);
                    Console.WriteLine("Reader {0} connected to database and reading something important", _index);
                    Thread.Sleep(random.Next(1000, 5000));
                    Connector.SendCommandFromClient((byte)Command.ReaderEndReading);
                    Console.WriteLine("Reader {0} finish reading and go sleep", _index);
                    Thread.Sleep(random.Next(5000, 10000));
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
              

            }
        }

        
    }
}
