﻿using System;
using System.IO;
using System.IO.Pipes;
using System.Threading;


namespace lab4
{
    class Connector
    {
        private static NamedPipeClientStream _clientConnection;
        private static NamedPipeServerStream _serverConnection;

        public static void CreateClientConnect()
        {
            _clientConnection = new NamedPipeClientStream(
                ".",
                Constants.PIPE_NAME,
                PipeDirection.InOut,
                PipeOptions.Asynchronous
            );
        }

        public static void CreateServerConnection()
        {
            _serverConnection = new NamedPipeServerStream(
                Constants.PIPE_NAME,
                PipeDirection.InOut,
                NamedPipeServerStream.MaxAllowedServerInstances,
                PipeTransmissionMode.Message,
                PipeOptions.Asynchronous
            );
        }

        public static void WaitForConnectionOnServer()
        {
            _serverConnection.WaitForConnection();
        }

        public static int ReadByteOnServer()
        {
            return _serverConnection.ReadByte();
        }

        public static void WaitForDrainOnServer()
        {
            _serverConnection.WaitForPipeDrain();
        }

        public static void WriteByteOnServer(byte value)
        {
            _serverConnection.WriteByte(value);
        }

        public static void ConnectAsClient()
        {
            try
            {
                _clientConnection.Connect();
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine(Constants.CANT_CONNECT);
                Thread.Sleep(1000);
                ConnectAsClient();
            }
        }

        public static int SendCommandFromClient(byte command)
        {
            CreateClientConnect();
            ConnectAsClient();
            _clientConnection.WriteByte(command);
            _clientConnection.WaitForPipeDrain();
            var response = _clientConnection.ReadByte();
            _clientConnection.WaitForPipeDrain();
            return response;
        }
    }
}
