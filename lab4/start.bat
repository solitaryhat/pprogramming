
set /a readers_count=10
set /a writers_count=5

start lab4\bin\Debug\lab4.exe server
for /l %%A in (1, 1, %readers_count%) do (
	start lab4\bin\Debug\lab4.exe reader %%A
)
for /l %%A in (1, 1, %writers_count%) do (
	start lab4\bin\Debug\lab4.exe writer %%A
)

pause

taskkill /im lab4.exe /f